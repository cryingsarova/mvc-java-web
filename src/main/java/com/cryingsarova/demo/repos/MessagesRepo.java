package com.cryingsarova.demo.repos;

import com.cryingsarova.demo.domain.Message;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MessagesRepo extends CrudRepository<Message, Long> {

    List<Message> findByComment(String comment);
}
